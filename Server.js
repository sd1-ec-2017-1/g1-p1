// recuperando os arquivos que serão utilizados pela classe
var net = require('net'),
User = require('./User'),
Canal = require('./Canal');

function Server(){
	this.users = [];	//array de usuarios
	this.canais = [];	// array de canais
	this.userid = 0;	//gerador de id
	this.client;
};

Server.initialize = function(){
	var server = new Server();		//Criando um objeto de server
	server.start();

	return server;			//chamo a funcao que inicia e retorno o objeto
};

//acessando o prototipo, inferindo em todas as derivações de server
Server.prototype = {
	//Inicia a conexao com client
	start : function(){
		var server = this;	//recebe o objeto dele mesmo quando foi chamado
		//criando um novo server de cliente
		this.server = net.createServer(function(client){ server.connection(client)});
		this.server.listen(6667);		//Colocano a porta em que o server vai ouvir
		console.log("Chat server running at port 6667\n");
	},

	connection : function(client){
		var server = this;
		server.client = client;
		user = new User(client, server);		//cria um novo objeto de usuario
		user.id = server.userid;
		user.nick = "guest_"+user.id;


		server.users[user.id] = user;
		server.userid++;

		client.on("data", function(text){
			
		var linhas = text.toString().split(/\n/); // pega a linha do texto e passa pra String
			for (var i = 0; i < linhas.length ; i++) {
				if(linhas[i])
					server.envia(user, linhas[i]);
			}

		});

	},

	envia : function(user, data){
		var server = this;
		var message = server.analisaData(data);		//recebe a mensagem tratada
		server.comandos(user, message);

	},

	analisaData : function(data){
		var server = this;
		//separando a mensagem enviada
		var message = data.trim().split(/ :/), 
					  //separando os argumentos da minha mensagem
					  args = message[0].split(' ');
		
		//deslocando a mensagem caso tenha o comando join
		message = [message.shift(), message.join(' :')];

		if(message.length > 0){
			args.push(message[1]);	//insere no array caso tenha mensagem
		}

		//pegando elementos de user, cortando a string onde tem os args e recolocando no array
		 if (data.match(/^:/)) {
            args[1] = args.splice(0, 1, args[1]);
            args[1] = (args[1] + '').replace(/^:/, '');
        }
         return {
            comando: args[0],		//retorna um objeto com o comando e os argumentos
            args: args.slice(1)
        };
    },
    
    comandos: function(user, message){
     	var server = this;
     	 if( message.comando == 'NICK') server.nick(user, message); //passa pra funcao nick
    	 else if( message.comando == 'QUIT') server.quit(user, message);	//passa para funcao quit
   		 else if( message.comando == 'PING') server.ping(user, message);	//passa para a funcao ping
   		 else if( message.comando == 'USER') server.user(user, message);	//passa para a funcao user
    	 else if( message.comando == 'JOIN') server.join(user, message); //passa para a funcao user
         else server.client.write("ERRO: comando inexistente\n");
    },

    nick: function(user, message){
     	  var server = this;
     	  if(!message.args[0]){
            server.client.write("ERRO: nickname está faltando\n");
            return false;
        }

     	var nick  = message.args[0];
   		//verifica o usuario atual e lanca uma exceçao caso está sendo utilizado

    	 for(var i=0; i<server.users.length; i++){
            
            anUser = server.users[i];
          	
            if((anUser != null)&&(anUser.nick.toUpperCase() === nick.toUpperCase())){
               server.client.write("ERRO: nickname já está sendo utilizado\n");
                return false;
            }
    	
     	}

        user.nick = nick;	//altera o nick do usuario
         server.client.write("OK: comando NICK executado com sucesso\n");
        return true;

    }, 

    quit : function(user, message){
        var server = this;

        var message = (message.args[0])? message.args[0] : "";	//recupera a mensagem para ser exibida
        
        if(!user){
            return false;
        }
       
        server.client.write(message + " : "+user.nick+" left this chat\n");
        server.client.write("OK: comando QUIT executado com sucesso\n");
        delete server.users[server.userid];	//deleta o user do array
        user.destroy();	//destroi a socket do usuario
    
    },

    ping : function(user, message){
     	var server = this;
     	server.client.write("PONG :"+message.args[0]+"\n");
     	server.client.write("OK: comando PING executado com sucesso\n");	//recebe a mensagem e retorna com um pong
    },

    user : function(user, message){
     	 var server = this;
     	 //verifica se o usuario está registrado
        
        if (user.registrado()) {
            server.client.write("ERRO: usuário já está registrado\n");
            return false;
        }
        //verifica se os parametros sao suficientes

         if ((!message.args[0])||(!message.args[4])) {
           server.client.write("ERRO: parâmetros faltando\n")
            return false;
        }
        user.username = message.args[0];
        user.realname = message.args[4];
        
       server.client.write("OK: username: "+user.username+" realname: "+user.realname+" nickname: "+user.nick+"\n");
        return true;
     },

    join : function(user, message) {  
        var server = this;
        var args = message.args;
        
        // sem argumentos
        if (!args[0]) {
            server.client.write("ERRO: parâmetros faltando\n");
            return false;
        }
        
        var channel = args[0];
        //pega o nome do canal 
        if (!channel[0].match(/[!#&+]/)) {
            channel = "#"+channel;
        }
        
        //se o canal existir adiciona o usuário
        if (server.getCanal(channel)) {
            //Se o usuário ainda não for um membro, adiciona
            if (!server.getCanal(channel).getUser(user.id)) {
                server.getCanal(channel).adicionaUser(user);
            }
            else{
                return false;
            }
        }
        else { //cria um novo canal e atribui o usuário como criador
            server.adicionaCanal(channel, user);
        }
        
        //mensagem de novo usuário para o canal
        var mensagem = ":"+user.nick+"!"+user.username+"@"+user.host+ "JOIN" + channel;
        server.getCanal(channel).write(mensagem, user);
        
        
        return true;
    },

    //recebe o nome do canal
    getCanal : function(id){
        
        var response = false;
        
        for (var i=0; i<this.canais.length; i++) {
            if(this.canais[i].nome === id){
                response = this.canais[i];
                break;
            }
        }
        return response;
    },

    //recebe o nome do canal e quem o criou
    adicionaCanal : function(nome, user){
        
        if(this.getCanal(nome) == false){ 
            this.canais.push(new Canal(this, nome, user));
            return true;
        }
        else return false;
    },

    getUser : function(id){
        
        var response = false;
        var server = this;
        
        
        if(isNaN(id)){
            for(var i=0; i<this.users.length; i++){
                if ((this.users[i]) && (this.users[i].get('nick') === id)) {
                    response = this.users[i];
                    break;
                }
            }
        }else{
            for (var i=0; i<this.users.length; i++) {
                if((this.users[i] != null) && this.users[i].id === id){
                    response = this.canais[i];
                    break;
                }
            }
        }
        
        return response;
    },


};
     }
}

Server.initialize();
