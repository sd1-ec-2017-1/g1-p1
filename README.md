# Servidor IRC NODE.js


## Primeriro Projeto de Sistemas Distribuídos 2017/1 

#### Membros:
* Higor Ferreira Pimentel
* Jean Jonatas
* Rachimi Solarevisky

---

#### Manual do Usuário   

#### Requisitos para execução
Para executar o servidor é necessária a instalação de algumas ferramentas em seu computador:

1. Node.js para execução do servidor, acesse [Node.js](https://nodejs.org) e saiba mais.   
2. Git para o download dos repositórios, acesse [Git website](https://git-scm.com) e saiba mais.        

#### Execução
Após isso deve-se seguir os seguintes passos:    

Para fazer o download dos arquivos necessários para a execução do servidor:  
    
    git clone https://gitlab.com/sd1-ec-2017-1/g1-p1.git
    
    
Acesse a pasta do projeto:

    cd g1-p1
    
Execute o servidor irc em segundo plano:

    node Server.js &
    
Agora acesse o servidor localmente através do telnet:

    telnet localhost 6667




#### Como utilizar 
Segue os comandos suportados pelo chat server:

* O comando NICK é utilizado para o registro do nickname do usuário:       


            NICK Fulano
    
    
* O comando USER registra o nome de usuário, o modo que ele deseja entrar no chat e seu nome real:


            USER  fulano 8 *  : Fulano Silva    
    

* O comando JOIN é usado para entrar em canal de chat:    

            JOIN #nome-do-canal-irc

* Para enviar mensagens no canal em que você está use o comando PRIVMSG seguido do canal e a mensagem:

            PRIVMSG #canal-irc :Olá, tudo bem ?

* Para avisar ao servidor que ainda está ativo no chat para que este não feche a conexão use o comando PING seguido do servidor:

			PING :freenode.net

* Para deixar o servidor use o comando QUIT opcionalmente seguido de uma mensagem de despedida:

            QUIT :Até mais galera!
