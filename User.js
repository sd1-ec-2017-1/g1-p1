
//classe do usuario, recebendo a socket e o servidor
var User = function(socket, server){
	this.server = server;		//recuperando o server

	this.socket = socket;		//socket do usuario
	this.host = '';				//host do usuario
	this.username = '';			//nome do usuario;
	this.realname = '';			//nome real
	this.nick = '';				//nick do user que será identificado
	this.canais = [];			//canais do usuario
	this.id = 0;			//id unico de usuario

	this.adicionaCanal = function (canal){
		this.canais.push(canal);		//adicionando o canal no array de canais 
	}	

	this.destroy = function(){
		this.socket.destroy();		//destroi a socket do usuario
	}

	this.registrado = function(){
        return (this.nick && this.username && this.realname)? true : false;		//verifica se um usuario esta registrado
    }
    
    
    this.write = function (mensagem) {
       
        this.socket.write(mensagem+"\r\n");
        
    };

};

module.exports = User;