//Classe dos canais recebe o servidor, nome do canal e o usuárioque o criou
function Canal(server, nome, op) {
    
    var server   = server; //recuperando o server
    this.nome    = nome;   //nome do canal
    this.users   = [];     //usuários do canal
    //this.opers   = [];     //operadores do canal
    this.criador = op.id;  //criador do canal
    


    //Adiciona um usuário à lista de usuários do canal
    this.adicionaUser = function(user){
    	
		this.users.push({
			id: user.id, 
			nickname: user.nick
		});
    	user.adicionaCanal(this.name);
    };

    //pega um usuário pelo seu ID ou Nickname
    this.getUser = function(id, attr){ 
    	
        var type = (typeof(id) == 'string')? 'nickname' : 'id';
        var response = false;
        for (var i=0; i<this.users.length; i++) {
            
            if (this.users[i][type] == id) {
                
                if (attr == 'index') {
                    response = i;
                }
                else if (attr) {
                    response = this.users[i][attr];
                }
                else response = this.users[i];
                break;
            }
        }
    	return response;
    };
    
    //remove um usuário do canal
    this.removeUser = function(user){
    	
    	
    	this.users.splice(this.getUser(user.id, 'index'), 1);
    	user.canais.splice(user.canais.indexOf(this.name), 1);
        if ((this.name[0] != "!") && (this.users.length == 0)) {
            server.removeChannel(this.name);
        }
    };
    
    //mensagem para todos os usuários do canal
    this.write = function(message, sender){
    	
        for (var i=0; i<this.users.length; i++) {
            
            user = this.users[i];
            
            if (user != null && user.id != 0) {
                            
                if((!sender)||(user.id != sender)){ //Não envia para o autor da mensagem
                
                    if(u = server.getUser(user.id))
                        u.write(message);
                }
            }
        }
    };
    
    //Adiciona o criador do canal na lista de membros e operadores
    this.adicionaUser(op);
    //this.addOper(op);
}
module.exports = Canal;
