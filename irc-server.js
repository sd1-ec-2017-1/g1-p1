// Load the TCP Library
net = require('net');
// Keep track of the chat clients
var clients = [];

// cria um objeto de nicknames
var nicks = {};

// Start a TCP Server
net.createServer(function (socket) {
  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort 
  // Put this new client in the list
  clients.push(socket);
  // Send a nice welcome message and announce
  socket.write("Welcome " + socket.name + "\n");
  broadcast(socket.name + " joined the chat\n", socket);
  // Handle incoming messages from clients.
  socket.on('data', function (data) {
    // broadcast(socket.name + "> " + data, socket);
    // analisar a mensagem
    analisar(data);
  });
  // Remove the client from the list when it leaves
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " left the chat.\n");
  });
  
  // Send a message to all clients
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client === sender) return;
      client.write(message);
    });
    // Log it to the server output too
    process.stdout.write(message)
  }
  function analisar(data) {
  	//quebrando a string em quebra de linha
  	var comandos = data.toString().split(/\n/);

  	for (var i = 0; i <comandos.length; i++) {
  		//recebendo os dados de iteração do vetor
  		comand = comandos[i];
  		
  		//quebra os primeiros parametros
  		var args = comand.split(/ :/)[0];

  		//caso haja uma mensagem, recupera
  		var message = comand.split(args + " :")[1];

  		//separa todos os argumentos e transforma em array
  		args = args.split(/ :/)[0].split(" ")

  		//se tiver mensagem, insere no array
  		if(message) args.push(message);
  	
  		if ( args[0] == "NICK" ) nick(args);
  		else if ( args[0] == "USER") user(args);
  		else if ( args[0] == "QUIT") quit(args);
  		else if ( args[0] == "JOIN") join(args);
        else socket.write("ERRO: comando inexistente\n");
  	}
  	
  }
 
  function nick(args) {

    socket.write("OK: comando NICK executado com sucesso\n");

    if( !args[1]){
    	socket.write("ERRO: nickname está faltando\n");
    	return;		//retorna a funcao
    }else if(nicks[ args[1]]){
    	socket.write("ERRO: nickname já existe\n");
    	return;
    }else{
    	if( socket.nick ){
    	//Verifica no vetor de 
    	for(var i = 0; i< nicks.lenght-1; i++){
    		if(nicks[i] == socket.nick)
    		delete nicks[i];
    	}
    	}
    	//registra nicks no objeto
		nicks[args[1]] = socket.name;
		//registra o nickname na socket
    	socket.nick = args[1];
    }
 
  
	//Comando executado
    socket.write("OK: comando NICK executado com sucesso\n");
  }
  function user(args) {
    //verifica se os 4 parametros foram inseridos
    


    socket.write("OK: comando USER executado com sucesso\n");
  }
  //encerra a socket com uma função de callback
  function quit(args){
  	//imprime a mensagem e encerra a socket
  	socket.write(args[0]);
  	socket.destroy();	
  		return;
  	}

function join(args) {
    socket.write("OK: comando JOIN executado com sucesso\n");
  }
}).listen(6667);
// Put a friendly message on the terminal of the server.
console.log("Chat server running at port 6667\n");
